package com.desafiobase2.dbsteps;

import com.desafiobase2.utils.DBUtils;
import com.desafiobase2.utils.Utils;

public class LimparDadosBancoDBSteps {

    private static String queriesPath = "src/test/java/com/desafiobase2/queries/limpardadosbanco/";

    public static void limparDadosBD() {
        bugFileTable();
        bugHistoryTable();
        bugMonitorTable();
        bugNoteTable();
        bugNoteTextTable();
        bugRelationshipTable();
        bugRevisionTable();
        bugTable();
        bugTagTable();
        bugTextTable();
        categoryTable();
        customFieldTable();
        emailTable();
        projectTable();
        tagTable();
        userTable();
    }

    private static void bugFileTable() {
        String query = Utils.getFileContent(queriesPath + "bugFileTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugHistoryTable() {
        String query = Utils.getFileContent(queriesPath + "bugHistoryTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugMonitorTable() {
        String query = Utils.getFileContent(queriesPath + "bugMonitorTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugNoteTable() {
        String query = Utils.getFileContent(queriesPath + "bugNoteTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugNoteTextTable() {
        String query = Utils.getFileContent(queriesPath + "bugNoteTextTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugRelationshipTable() {
        String query = Utils.getFileContent(queriesPath + "bugRelationshipTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugRevisionTable() {
        String query = Utils.getFileContent(queriesPath + "bugRevisionTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugTable() {
        String query = Utils.getFileContent(queriesPath + "bugTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugTagTable() {
        String query = Utils.getFileContent(queriesPath + "bugTagTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void bugTextTable() {
        String query = Utils.getFileContent(queriesPath + "bugTextTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void categoryTable() {
        String query = Utils.getFileContent(queriesPath + "categoryTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void customFieldTable() {
        String query = Utils.getFileContent(queriesPath + "customFieldTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void emailTable() {
        String query = Utils.getFileContent(queriesPath + "emailTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void projectTable() {
        String query = Utils.getFileContent(queriesPath + "projectTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void tagTable() {
        String query = Utils.getFileContent(queriesPath + "tagTable.sql");
        DBUtils.executeUpdate(query);
    }

    private static void userTable() {
        String query = Utils.getFileContent(queriesPath + "userTable.sql");
        DBUtils.executeUpdate(query);
    }
}
