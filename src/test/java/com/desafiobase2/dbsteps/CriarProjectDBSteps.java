package com.desafiobase2.dbsteps;

import com.desafiobase2.utils.DBUtils;
import com.desafiobase2.utils.Utils;

public class CriarProjectDBSteps {

    private static String queriesPath = "src/test/java/com/desafiobase2/queries/gerenciar/";

    public static void criarProjectDB(){
        String query = Utils.getFileContent(queriesPath + "criarProject.sql");

        DBUtils.executeUpdate(query);
    }
}
