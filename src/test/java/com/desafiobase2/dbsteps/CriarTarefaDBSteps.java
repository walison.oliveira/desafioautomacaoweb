package com.desafiobase2.dbsteps;

import com.desafiobase2.utils.DBUtils;
import com.desafiobase2.utils.Utils;

public class CriarTarefaDBSteps {

    private static String queriesPath = "src/test/java/com/desafiobase2/queries/tarefa/";

    public static void criarTarefaDB() {
        criarTarefa();
        criarDescricaoTarefa();
    }

    public static void criarTarefaFechadaDB() {
        criarTarefaFechada();
        criarDescricaoTarefa();
        criarFechamentoTarefa();
    }

    private static void criarTarefa() {
        String query = Utils.getFileContent(queriesPath + "criarTarefa.sql");

        DBUtils.executeUpdate(query);
    }

    private static void criarDescricaoTarefa() {
        String query = Utils.getFileContent(queriesPath + "criarDescricaoTarefa.sql");

        DBUtils.executeUpdate(query);
    }

    private static void criarTarefaFechada() {
        String query = Utils.getFileContent(queriesPath + "criarTarefaFechada.sql");

        DBUtils.executeUpdate(query);
    }

    private static void criarFechamentoTarefa() {
        String query = Utils.getFileContent(queriesPath + "criarFechamentoTarefa.sql");

        DBUtils.executeUpdate(query);
    }
}
