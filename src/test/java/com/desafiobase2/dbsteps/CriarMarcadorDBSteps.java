package com.desafiobase2.dbsteps;

import com.desafiobase2.utils.DBUtils;
import com.desafiobase2.utils.Utils;

public class CriarMarcadorDBSteps {

    private static String queriesPath = "src/test/java/com/desafiobase2/queries/gerenciar/";

    public static void criarMarcadorDB(){
        String query = Utils.getFileContent(queriesPath + "criarMarcador.sql");

        DBUtils.executeUpdate(query);
    }
}
