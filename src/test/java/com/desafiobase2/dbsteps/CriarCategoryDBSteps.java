package com.desafiobase2.dbsteps;

import com.desafiobase2.utils.DBUtils;
import com.desafiobase2.utils.Utils;

public class CriarCategoryDBSteps {

    private static String queriesPath = "src/test/java/com/desafiobase2/queries/gerenciar/";

    public static void criarCategoryDB(){
        String query = Utils.getFileContent(queriesPath + "criarCategory.sql");

        DBUtils.executeUpdate(query);
    }
}
