package com.desafiobase2.dbsteps;

import com.desafiobase2.utils.DBUtils;
import com.desafiobase2.utils.Utils;

public class CriarAnotacaoDBSteps {

    private static String queriesPath = "src/test/java/com/desafiobase2/queries/tarefa/";

    public static void criarAnotacaoDB(){
        criarAnotacao();
        criarTextoAnotacao();
    }

    private static void criarAnotacao(){
        String query = Utils.getFileContent(queriesPath + "criarAnotacao.sql");

        DBUtils.executeUpdate(query);
    }

    private static void criarTextoAnotacao(){
        String query = Utils.getFileContent(queriesPath + "criarTextoAnotacao.sql");

        DBUtils.executeUpdate(query);
    }
}
