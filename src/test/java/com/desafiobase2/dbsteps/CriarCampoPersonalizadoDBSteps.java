package com.desafiobase2.dbsteps;

import com.desafiobase2.utils.DBUtils;
import com.desafiobase2.utils.Utils;

public class CriarCampoPersonalizadoDBSteps {

    private static String queriesPath = "src/test/java/com/desafiobase2/queries/gerenciar/";

    public static void criarCampoPersonalizadoDB(){
        String query = Utils.getFileContent(queriesPath + "criarCampoPersonalizado.sql");

        DBUtils.executeUpdate(query);
    }
}
