package com.desafiobase2.dbsteps;

import com.desafiobase2.utils.DBUtils;
import com.desafiobase2.utils.Utils;

public class CriarUsuarioDBSteps {

    private static String queriesPath = "src/test/java/com/desafiobase2/queries/gerenciar/";

    public static void criarUsuarioDB() {
        String query = Utils.getFileContent(queriesPath + "inserirUsuario.sql");
        DBUtils.executeUpdate(query);
    }
}
