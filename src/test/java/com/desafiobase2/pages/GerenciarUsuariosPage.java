package com.desafiobase2.pages;

import com.desafiobase2.bases.PageBase;
import org.openqa.selenium.By;

public class GerenciarUsuariosPage extends PageBase {
    //Mapping
    By criarNovaContaButton = By.xpath("//a[@class='btn btn-primary btn-white btn-round btn-sm']");
    By mostrarDesativadosCheckbox = By.xpath("//span[text()='Mostrar desativados']");
    By aplicarFiltroButton = By.xpath("//input[@value='Aplicar Filtro']");
    By nomeUsuarioTextField = By.id("user-username");
    By nomeVerdadeiroTextField = By.id("user-realname");
    By emailTextField = By.id("email-field");
    By nivelAcessoCombobox = By.id("user-access-level");
    By criarUsuarioButton = By.xpath("//input[@value='Criar Usuário']");
    By mensagemSucesso = By.xpath("//p[@class='bold bigger-110']");
    By mensagemError = By.xpath("//p[@class='bold']");
    By apagarUsuarioButton = By.xpath("//input[@value='Apagar Usuário']");
    By apagarContaButton = By.xpath("//input[@value='Apagar Conta']");
    By atualizarUsuarioButton = By.xpath("//input[@value='Atualizar Usuário']");
    By messageSucesso = By.xpath("//p[@class='bold bigger-110']");
    By habilitadoCheckbox = By.xpath("//form[@id='edit-user-form']//span[@class='lbl']");
    By editNomeUsuarioTextField = By.id("edit-username");
    By editNivelAcessoCombobox = By.id("edit-access-level");

    //Actions
    public void clickCriarNovaContaButton() {
        click(criarNovaContaButton);
    }

    public void clickUsuario(String usuario) {
        click(By.xpath("//a[text()='" + usuario + "']"));
    }

    public void clickMostrarDesativados() {
        click(mostrarDesativadosCheckbox);
    }

    public void clickAplicarFiltroButton() {
        click(aplicarFiltroButton);
    }

    public String returnUsuario(String usuario) {
        return getText(By.xpath("//a[text()='" + usuario + "']"));
    }

    public void sendKeysNomeUsuarioTextField(String nomeUsuario) {
        sendKeys(nomeUsuarioTextField, nomeUsuario);
    }

    public void sendKeysNomeVerdadeiro(String nomeVerdadeiro) {
        sendKeys(nomeVerdadeiroTextField, nomeVerdadeiro);
    }

    public void sendKeysEmailTextField(String email) {
        sendKeys(emailTextField, email);
    }

    public void selecionarNivelAcessoCombobox(String nivelAcesso){
        comboBoxSelectByVisibleText(nivelAcessoCombobox, nivelAcesso);
    }

    public void clickCriarUsuarioButton() {
        click(criarUsuarioButton);
    }

    public String returnMensagemSucesso() {
        return getText(mensagemSucesso);
    }

    public String returnMensagemError() {
        return getText(mensagemError);
    }

    public void clickApagarUsuarioButton() {
        click(apagarUsuarioButton);
    }

    public void clickApagarContaButton(){
        click(apagarContaButton);
    }

    public String returnMessageSucesso() {
        return getText(messageSucesso);
    }

    public void clickHabilitadoCheckBox(){
        click(habilitadoCheckbox);
    }

    public void clickAtualizarUsarioButton(){
        click(atualizarUsuarioButton);
    }

    public void sendKeysEditNomeUsuarioTextField(String nomeUsuario){
        clearAndSendKeys(editNomeUsuarioTextField, nomeUsuario);
    }

    public void selecionarNivelAcessoUsuario(String nivelAcesso){
        comboBoxSelectByVisibleText(editNivelAcessoCombobox, nivelAcesso);
    }

}
