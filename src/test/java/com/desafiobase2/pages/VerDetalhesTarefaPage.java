package com.desafiobase2.pages;

import com.desafiobase2.bases.PageBase;
import org.openqa.selenium.By;

public class VerDetalhesTarefaPage extends PageBase {
    //Mapping
    By atualizarButton = By.xpath("//input[@value='Atualizar']");
    By apagarButton = By.xpath("//input[@value='Apagar']");
    By atribuidoAText = By.xpath("//td[@class='bug-assigned-to']/a");
    By fecharButton = By.xpath("//input[@value='Fechar']");
    By estadoTarefaText = By.xpath("//td[@class='bug-status']");
    By alterarStatusCombobox = By.xpath("//select[@name='new_status']");
    By alterarStatusTarefaButton = By.xpath("//input[@value='Alterar Status:']");
    By reabrirTarefaButton = By.xpath("//input[@value='Reabrir']");
    By solicitarRetornoButton = By.xpath("//input[@value='Solicitar Retorno']");
    By numeroTarefaModificada = By.xpath("//td[@class='bug-id']");
    By monitorarTarefaButton = By.xpath("//input[@value='Monitorar']");
    By marcarComoPegajosoButton = By.xpath("//input[@value='Marcar como Pegajoso']");
    By desmarcarComoPegajosoButton = By.xpath("//input[@value='Desmarcar como Pegajoso']");
    By enviarLembreteButton = By.xpath("//div[@class='btn-group pull-left']/a[text()='Enviar um lembrete']");
    By irParaAnotacoesButton = By.xpath("//div[@class='btn-group pull-left']/a[text()='Ir para as Anotações']");
    By apagarAtividadesButton = By.xpath("//div[@id='bugnotes']//button[text()='Apagar']");
    By atividades = By.xpath("//div[@id='bugnotes']//td[@class='bugnote-note bugnote-public']");
    By irParaHistoricoButton = By.xpath("//div[@class='btn-group pull-left']/a[text()='Ir para o Histórico']");
    By resolucaoTarefaCombobox = By.xpath("//select[@name='resolution']");
    By resolverTarefaButton = By.xpath("//input[@value='Resolver Tarefa']");
    By historicoAtividadeText = By.xpath("//div[@id='history']");
    By usuarioMonitorandoText = By.xpath("//div[@id='monitoring']//a[text()='administrator']");
    By historicoTarefaText = By.xpath("//div[@id='history']//h4[@class='widget-title lighter']");
    By fecharTarefaButton = By.xpath("//input[@value='Fechar Tarefa']");
    By adicionarAnotacaoTextField = By.id("bugnote_text");
    By selectUserText = By.xpath("//select[@id='recipient']/option");
    By enviarButton = By.xpath("//input[@value='Enviar']");
    By messageSucesso = By.xpath("//p[@class='bold bigger-110']");
    By messageError = By.xpath("//p[@class='bold']");
    By atividadesInformacao = By.xpath("//div[@id='bugnotes']//tbody");
    By estadoCheckbox = By.id("status");
    By numeroTarefaText = By.xpath("//td[@class='bug-id']");
    By anotacaoTextField = By.id("bugnote_text");
    By adicionarAnotacaoButton = By.xpath("//input[@value='Adicionar Anotação']");
    By atividadeText = By.xpath("//div[@id='bugnotes']//td[@class='bugnote-note bugnote-public']");
    By apagarAnotacaoButton = By.xpath("//input[@value='Apagar Anotação']");
    By naoHaAnotacaoText = By.xpath("//div[@id='bugnotes']//tr[@class='bugnotes-empty']");
    By apagarTarefasButton = By.xpath("//input[@value='Apagar Tarefas']");
    By salvarTarefaButton = By.xpath("//input[@type='submit']");
    By atribuirACombobox = By.id("handler_id");

    //Actions
    public void clickAtualizarTarefaButton() {
        click(atualizarButton);
    }

    public void clickDeletarTarefaButton() {
        click(apagarButton);
    }

    public String returnAtribuidoA() {
        return getText(atribuidoAText);
    }

    public void clickFecharButton() {
        click(fecharButton);
    }

    public String returnEstadoTarefa() {
        return getText(estadoTarefaText);
    }

    public void selecionarAlterarStatusCombobox(String status) {
        comboBoxSelectByVisibleText(alterarStatusCombobox, status);
    }

    public void clickAlterarStatusTarefaButton() {
        click(alterarStatusTarefaButton);
    }

    public void clickReabrirTarefaButton() {
        click(reabrirTarefaButton);
    }

    public String returnNumeroTarefaModificada() {
        return getText(numeroTarefaModificada);
    }

    public void clickMarcarComoPegajosoButton() {
        click(marcarComoPegajosoButton);
    }

    public String clickDesmarcarPegajosoButton() {
        return getText(desmarcarComoPegajosoButton);
    }

    public void clickMonitorarTarefaButton() {
        click(monitorarTarefaButton);
    }

    public void clickEnviarLembreteButton() {
        click(enviarLembreteButton);
    }

    public void clickIrParaAnotacoesButton() {
        click(irParaAnotacoesButton);
    }

    public void clickApagarAtividadesButton() {
        click(atividades);
        click(apagarAtividadesButton);
    }

    public void clickIrParaHistoricoButton() {
        click(irParaHistoricoButton);
    }

    public void selecionarResolucaoTarefaComboBox(String resolucaoTarefa){
        comboBoxSelectByVisibleText(resolucaoTarefaCombobox, resolucaoTarefa);
    }

    public void clickResolverTarefaButton(){
        click(resolverTarefaButton);
    }

    public void clickSolicitarRetornoButton(){
        click(solicitarRetornoButton);
    }

    public String returnMensagemHistoricoAtividade(){
        return getText(historicoAtividadeText);
    }

    public String returnUsuarioMonitorando(){
        return getText(usuarioMonitorandoText);
    }

    public String returnCampoHistoricoTarefa(){
        return getText(historicoTarefaText);
    }

    public void clickFecharTarefaButton() {
        click(fecharTarefaButton);
    }

    public void preencherAdicionarAnotacao(String adicionarAnotacao) {
        clearAndSendKeys(adicionarAnotacaoTextField, adicionarAnotacao);
    }

    public void clickUserText(){
        click(selectUserText);
    }

    public void clickEnviarButton(){
        click(enviarButton);
    }

    public String returnMessageSucesso(){
        return getText(messageSucesso);
    }

    public String returnMessageError(){
        return getText(messageError);
    }

    public String returnAtividadesInformacao(){
        return getText(atividadesInformacao);
    }

    public void selecionarEstado(String estado){
        comboBoxSelectByVisibleText(estadoCheckbox, estado);
    }

    public String returnEstadoTarefa(String estado){
        return getText(By.xpath(" //td[text()=' "+ estado +"']"));
    }

    public String returnNumeroTarefa(){
        return getText(numeroTarefaText);
    }

    public void senKeysAnotacaoTextField(String anotacao){
        sendKeys(anotacaoTextField, anotacao);
    }

    public void clickAdicionarAnotacaoButton(){
        click(adicionarAnotacaoButton);
    }

    public String retunTextoAtividades(){
        return getText(atividadeText);
    }

    public void clickApagarAnotacaoButton(){

        click(apagarAnotacaoButton);
    }

    public String returnMessageNaoHaAnotacao(){
        return getText(naoHaAnotacaoText);
    }

    public void clickApagarTarefaButton(){
        click(apagarTarefasButton);
    }

    public void clickSalvarTarefaButton() {
        click(salvarTarefaButton);
    }

    public void selecionarAtribuirA(String atribuirA) {
        comboBoxSelectByVisibleText(atribuirACombobox, atribuirA);
    }
}
