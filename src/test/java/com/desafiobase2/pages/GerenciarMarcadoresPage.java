package com.desafiobase2.pages;

import com.desafiobase2.bases.PageBase;
import org.openqa.selenium.By;

public class GerenciarMarcadoresPage extends PageBase {
    //Mapping
    By criarMarcadorButton = By.xpath("//input[@type='submit']");
    By nomeMarcadorLinkText = By.xpath("//div[@id='main-container']//td/a");
    By nomeMarcadorTextField = By.id("tag-name");
    By apagarMarcadorButton = By.xpath("//input[@value='Apagar Marcador']");
    By informacoesMarcadorText = By.xpath("//div[@id='main-container']//tbody");
    By atualizarMarcadorButton = By.xpath("//input[@value='Atualizar Marcador']");

    //Actions
    public void clickCriarMarcador() {
        click(criarMarcadorButton);
    }

    public String returnNomeMarcador(String nomeMarcador) {
        return getText(By.xpath("//a[text()='" + nomeMarcador + "']"));
    }

    public void clickNomeMarcadorLinkText() {
        click(nomeMarcadorLinkText);
    }

    public void sendKeysNomeMarcador(String nomeMarcador) {
        sendKeys(nomeMarcadorTextField, nomeMarcador);
    }

    public void clickApagarMarcadorButton() {
        click(apagarMarcadorButton);
    }

    public String returnInformacoesMarcadorText() {
        return getText(informacoesMarcadorText);
    }

    public void clickAtualizarMarcadorButton() {
        click(atualizarMarcadorButton);
    }

    public void sendKeysNomeMarcadorTextField(String nomeMarcador) {
        clearAndSendKeys(nomeMarcadorTextField, nomeMarcador);
    }

    public String returnNomeMarcadorAlterado(String nomeMarcador) {
        return getText(By.xpath("//div[@id='main-container']//td[text()='" + nomeMarcador + "']"));
    }
}
