package com.desafiobase2.pages;

import com.desafiobase2.bases.PageBase;
import org.openqa.selenium.By;

public class VerTarefasPage extends PageBase {
    //Mapping
    By redefinirButton = By.xpath("//div[@class='btn-group pull-left']/a[@href='view_all_set.php?type=0']");
    By tarefaLinkText = By.xpath("//td[@class='column-id']/a");
    By estadoTarefaLinkText = By.id("show_status_filter");
    By estadoCombobox = By.xpath("//select[@name='status[]']");
    By aplicarFiltroButton = By.xpath("//input[@value='Aplicar Filtro']");
    By tarefasInfomacoesTable = By.xpath("//table[@id='buglist']/tbody");


    //Actions
    public void clickRedefinirButton (){
        click(redefinirButton);
    }

    public void clickTarefaLinkText(){
        click(tarefaLinkText);
    }

    public void clickEstadoTarefaLinkText(){
        click(estadoTarefaLinkText);
    }

    public void selecionarEstadoComboBox(String estado){
        comboBoxSelectByVisibleText(estadoCombobox, estado);
    }

    public void clickAplicarFiltroButton(){
        click(aplicarFiltroButton);
    }

    public String returnTarefasInformacoesTable(){
        return getText(tarefasInfomacoesTable);
    }
}
