package com.desafiobase2.pages;

import com.desafiobase2.bases.PageBase;
import org.openqa.selenium.By;

public class LoginPage extends PageBase {
    //Mapping
    By usernameTextField = By.id("username");
    By passwordTextField = By.id("password");
    By loginButton = By.xpath("//input[@type='submit']");
    By measageErroText = By.xpath("//div[@class='alert alert-danger']");

    //Actions
    public void sendKeysUsernameTextField(String usuario){
        SendKeysJavaScript(usernameTextField, usuario);
    }

    public void senKeysPasswordTextField(String senha){
        SendKeysJavaScript(passwordTextField, senha);
    }

    public void clickLoginButton(){
        ClickJavaScript(loginButton);
    }

    public String returnMessageDeErro(){
        return getText(measageErroText);
    }
}