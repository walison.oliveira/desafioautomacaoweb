package com.desafiobase2.pages;

import com.desafiobase2.bases.PageBase;
import org.openqa.selenium.By;

public class MainPage extends PageBase {
    //Mapping
    By usernameLink = By.linkText("administrator");
    By criarTarefaButton = By.xpath("//i[@class='menu-icon fa fa-edit']");
    By verTarefasButton = By.xpath("//i[@class='menu-icon fa fa-list-alt']");
    By minhaVisaoButton = By.xpath("//i[@class='menu-icon fa fa-dashboard']");
    By gerenciarButton = By.xpath("//i[@class='menu-icon fa fa-gears']");

    //Actions
    public String returnUsernameInformacoesDeLogin() {
        return getText(usernameLink);
    }

    public void clickCriarTarefaButton() {
        click(criarTarefaButton);
    }

    public void clickVerTarefasButton() {
        click(verTarefasButton);
    }

    public void clickMinhaVisaoButton() {
        click(minhaVisaoButton);
    }

    public void clickGerenciarButton(){
        click(gerenciarButton);
    }

}