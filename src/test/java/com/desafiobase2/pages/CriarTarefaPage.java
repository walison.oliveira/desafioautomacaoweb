package com.desafiobase2.pages;

import com.desafiobase2.bases.PageBase;
import org.openqa.selenium.By;

public class CriarTarefaPage extends PageBase {
    //Mapping
    By selecionarProjetoButton = By.xpath("//input[@type='submit']");
    By categoriaComboBox = By.id("category_id");
    By frequenciaCombobox = By.id("reproducibility");
    By gravidadeCombobox = By.id("severity");
    By prioridadeCombobox = By.id("priority");
    By atribuirACombobox = By.id("handler_id");
    By resumoTextField = By.id("summary");
    By descricaoTextField = By.id("description");
    By passosParaReproduzirTextField = By.id("steps_to_reproduce");
    By informacoesAdicionaisTextField = By.id("additional_info");
    By salvarTarefaButton = By.xpath("//input[@type='submit']");
    By mensagemSucesso = By.xpath("//p[@class='bold bigger-110']");
    By messageError = By.xpath("//div[@class='error-info']");
    By atribuidoAText = By.xpath("//td[@class='bug-assigned-to']");
    By numeroTarefaText = By.xpath("//td[@class='bug-id']");

    //Actions
    public void clickSelecionarProjetoButton(){
        click(selecionarProjetoButton);
    }

    public void selecionarCategoria(String categoria) {
        comboBoxSelectByVisibleText(categoriaComboBox, categoria);
    }

    public void selecionarFrequencia(String frequencia) {
        comboBoxSelectByVisibleText(frequenciaCombobox, frequencia);
    }

    public void selecionarGravidade(String gravidade) {
        comboBoxSelectByVisibleText(gravidadeCombobox, gravidade);
    }

    public void selecionarPrioridade(String prioridade) {
        comboBoxSelectByVisibleText(prioridadeCombobox, prioridade);
    }

    public void selecionarAtribuirA(String atribuirA) {
        comboBoxSelectByVisibleText(atribuirACombobox, atribuirA);
    }

    public void sendKeysResumoTextField(String resumo) {
        clearAndSendKeys(resumoTextField, resumo);
    }

    public void sendKeysDescricaoTextField(String descricao) {
        clearAndSendKeys(descricaoTextField, descricao);
    }

    public void sendKeysPassosParaReproduzirTextField(String passosParaReproduzir) {
        clearAndSendKeys(passosParaReproduzirTextField, passosParaReproduzir);
    }

    public void sendKeysInformacoesAdicionaisTextField(String informacoesAdicionais) {
        clearAndSendKeys(informacoesAdicionaisTextField, informacoesAdicionais);
    }

    public void clickSalvarTarefaButton() {
        click(salvarTarefaButton);
    }

    public String returnMessageSucesso() {
        return getText(mensagemSucesso);
    }

    public String returnAtribuidoAText() {
        return getText(atribuidoAText);
    }

    public String returnNumeroTarefa(){
        return getText(numeroTarefaText);
    }
}
