package com.desafiobase2.pages;

import com.desafiobase2.bases.PageBase;
import org.openqa.selenium.By;

public class GerenciarPage extends PageBase {
    //Mapping
    By gerenciarUsuariosButton = By.xpath("//a[text()='Gerenciar Usuários']");
    By gerenciarProjetosButton = By.xpath("//a[text()='Gerenciar Projetos']");
    By gerenciarMarcadoresButton = By.xpath("//a[text()='Gerenciar Marcadores']");
    By gerenciarCamposPersonalizadosButton = By.xpath("//a[text()='Gerenciar Campos Personalizados']");

    //Actions
    public void clickGerenciarUsuariosButton(){
        click(gerenciarUsuariosButton);
    }

    public void clickGerenciarProjetosButton(){
        click(gerenciarProjetosButton);
    }

    public void clickGerenciarMarcadoresButton(){
        click(gerenciarMarcadoresButton);
    }

    public void clickGerenciarCamposPersonalizadosButton(){
        click(gerenciarCamposPersonalizadosButton);
    }
}
