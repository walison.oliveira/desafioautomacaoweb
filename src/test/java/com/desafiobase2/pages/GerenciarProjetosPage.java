package com.desafiobase2.pages;

import com.desafiobase2.bases.PageBase;
import org.openqa.selenium.By;

public class GerenciarProjetosPage extends PageBase {
    //Mapping
    By criarNovoProjetoButton = By.xpath("//button[text()='Criar Novo Projeto']");
    By alterarCategoriaButton = By.xpath("//button[text()='Alterar']");
    By apagarCategoriaButton = By.xpath("//button[text()='Apagar']");
    By projetoNomeLinkText = By.xpath("//div[@id='main-container']//td/a");
    By nomeProjetoTextField = By.id("project-name");
    By adicionarProjeto = By.xpath("//input[@value='Adicionar projeto']");
    By messageSucesso = By.xpath("//p[@class='bold bigger-110']");
    By nomeCategoriaTextField = By.xpath("//input[@name='name']");
    By adicionarCategoriaButton = By.xpath("//input[@value='Adicionar Categoria']");
    By messageError = By.xpath("//p[@class='bold']");
    By estadoCheckBox = By.id("project-status");
    By atualizarProjetoButton = By.xpath("//input[@value='Atualizar Projeto']");
    By apagarProjetoButton = By.xpath("//input[@value='Apagar Projeto']");
    By informacoesProjetoText = By.xpath("//div[@id='main-container']//tbody");
    By confirmarApagarCategoriaButton = By.xpath("//input[@value='Apagar Categoria']");
    By categoriaTextField = By.id("proj-category-name");
    By atualizarCategoriaButton = By.xpath("//input[@value='Atualizar Categoria']");

    //Actions
    public void clickCriarNovoProjetoButton() {
        click(criarNovoProjetoButton);
    }

    public void clickProjetoLinkText() {
        click(projetoNomeLinkText);
    }

    public String returnNomeProjeto(String nomeProjeto) {
        return getText(By.xpath("//a[text()='" + nomeProjeto + "']"));
    }

    public void clickAlterarCategoriaButton() {
        click(alterarCategoriaButton);
    }

    public void clickApagarCategoriaButton() {
        click(apagarCategoriaButton);
    }

    public void sendKeysNomeProjetoTextField(String nomeProjeto) {
        clearAndSendKeys(nomeProjetoTextField, nomeProjeto);
    }

    public void clickAdicionarProjeto() {
        click(adicionarProjeto);
    }

    public String returnMensagemSucesso() {
        return getText(messageSucesso);
    }

    public void sendKeysNomeCategoria(String nomeCategoria) {
        clearAndSendKeys(nomeCategoriaTextField, nomeCategoria);
    }

    public void clickAdicionarCategoria() {
        click(adicionarCategoriaButton);
    }

    public String returnNomeCategoria(String nomeCategoria) {
        return getText(By.xpath("//div[@id='categories']//td[text()='" + nomeCategoria + "']"));
    }

    public String returnMessageError() {
        return getText(messageError);
    }

    public void selecionarEstadoProejeto(String estadoProjeto) {
        comboBoxSelectByVisibleText(estadoCheckBox, estadoProjeto);
    }

    public void clickAtualizarProjetoButton() {
        click(atualizarProjetoButton);
    }

    public void clickApagarProjetoButton() {
        click(apagarProjetoButton);
    }

    public String returnInformacoesProjeto() {
        return getText(informacoesProjetoText);
    }

    public void clickConfirmarApagarCategoria() {
        click(confirmarApagarCategoriaButton);
    }

    public void sendKeysCategoria(String categoria) {
        clearAndSendKeys(categoriaTextField, categoria);
    }

    public void clickAtualizarCategoria() {
        click(atualizarCategoriaButton);
    }
}
