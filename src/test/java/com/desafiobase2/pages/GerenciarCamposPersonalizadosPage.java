package com.desafiobase2.pages;

import com.desafiobase2.bases.PageBase;
import org.openqa.selenium.By;

public class GerenciarCamposPersonalizadosPage extends PageBase {
    //Mapping
    By nomeCampoPersolizadoLinkText = By.xpath("//div[@id='main-container']//td/a");
    By informacoesCampoPersonalizadoText = By.xpath("//div[@class='widget-body']//tbody");
    By nomeCampoPersonalizadoTextField = By.xpath("//input[@name='name']");
    By novoCampoPersonalizadoButton = By.xpath("//input[@value='Novo Campo Personalizado']");
    By apagarCampoPersonalizadoButton = By.xpath("//input[@value='Apagar Campo Personalizado']");
    By apagarCampoButton = By.xpath("//input[@value='Apagar Campo']");
    By atualizarCampoPersonalizadoButton = By.xpath("//input[@value='Atualizar Campo Personalizado']");
    By messageSucesso = By.xpath("//p[@class='bold bigger-110']");
    By messageError = By.xpath("//p[@class='bold']");

    //Actions
    public void clickNomeCampoPersolizadoLinkText() {
        click(nomeCampoPersolizadoLinkText);
    }

    public String returnNomeCampoPersonalizado(String nomeCampoPersonalizado) {
        return getText(By.xpath("//div[@id='main-container']//a[text()='" + nomeCampoPersonalizado + "']"));
    }

    public String returnInformacoesCampoPersonalizadoText() {
        return getText(informacoesCampoPersonalizadoText);
    }

    public void sedKeysNomeCampoPersonalizadoTextField(String nomeCampoPersonalizado) {
        sendKeys(nomeCampoPersonalizadoTextField, nomeCampoPersonalizado);
    }

    public void clickNovoCampoPersonalizadoButton() {
        click(novoCampoPersonalizadoButton);
    }

    public void clickApagarCampoPersonalizadoButton() {
        click(apagarCampoPersonalizadoButton);
    }

    public void clickApagarCampoButton() {
        click(apagarCampoButton);
    }

    public void sendKeysNomeCampoPersonalizadoTextField(String nomeCampoPersonalizado) {
        clearAndSendKeys(nomeCampoPersonalizadoTextField, nomeCampoPersonalizado);
    }

    public void clickAtualizarCampoPersonalizadoButton() {
        click(atualizarCampoPersonalizadoButton);
    }

    public String returnMensagemSucesso() {
        return getText(messageSucesso);
    }

    public String returnMensagemError() {
        return getText(messageError);
    }
}