package com.desafiobase2.pages;

import com.desafiobase2.bases.PageBase;
import org.openqa.selenium.By;

public class MinhaVisaoPage extends PageBase {
    //Mapping

    //Actions
    public String returnNumeroTarefaModificada(String numerotarefa) {
        return getText(By.xpath("//div[@id='recent_mod']//a[text()='" + numerotarefa + "']"));
    }

    public String returnTarefaNaoAtribuida(String tarefa) {
        return getText(By.xpath("//div[@id='unassigned']//a[text()='" + tarefa + "']"));
    }


    public String returnTarefasAtribuidosAMim(String tarefa) {
        return getText(By.xpath("//div[@id='assigned']//a[text()='" + tarefa + "']"));
    }

    public String returnTarefaResolvida(String numeroTarefa){
        return getText(By.xpath("//div[@id='resolved']//a[text()='"+numeroTarefa+"']"));
    }

    public String returnTarefaMonitorada(String numeroTarefa){
        return getText(By.xpath("//div[@id='monitored']//a[text()='"+numeroTarefa+"']"));
    }
}
