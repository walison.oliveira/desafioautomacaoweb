package com.desafiobase2.flows;

import com.desafiobase2.pages.LoginPage;

public class LoginFlows {
    //Objects and constructor
    LoginPage loginPage;

    public LoginFlows(){
        loginPage = new LoginPage();
    }

    //Flows
    public void efetuarLogin(String username, String password){
        loginPage.sendKeysUsernameTextField(username);
        loginPage.clickLoginButton();
        loginPage.senKeysPasswordTextField(password);
        loginPage.clickLoginButton();
    }
}