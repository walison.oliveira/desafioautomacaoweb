package com.desafiobase2.tests;

import com.desafiobase2.bases.TestBase;
import com.desafiobase2.dbsteps.CriarTarefaDBSteps;
import com.desafiobase2.flows.LoginFlows;
import com.desafiobase2.pages.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class VerTarefaTests extends TestBase {
    //Objects
    LoginFlows loginFlows;
    MainPage mainPage;
    VerTarefasPage verTarefasPage;
    CriarTarefaPage criarTarefaPage1;
    VerDetalhesTarefaPage verDetalhesTarefaPage;
    MinhaVisaoPage minhaVisaoPage;

    @Test
    public void adicionarAnotacaoComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String anotacao = "Anotacão";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickIrParaAnotacoesButton();
        verDetalhesTarefaPage.senKeysAnotacaoTextField(anotacao);
        verDetalhesTarefaPage.clickAdicionarAnotacaoButton();

        Assert.assertEquals(anotacao, verDetalhesTarefaPage.retunTextoAtividades());
    }

    @Test
    public void adicionarAnotacaoComOcampoAnotacaoVazio() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickIrParaAnotacoesButton();
        verDetalhesTarefaPage.clickAdicionarAnotacaoButton();

        Assert.assertEquals("APPLICATION ERROR #11", verDetalhesTarefaPage.returnMessageError());
    }

    @Test
    public void apagarAnotacaoComSucesso() {
        //Preparar os dados
        //CriarAnotacaoDBSteps.criarAnotacaoDB();

        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickApagarAtividadesButton();
        verDetalhesTarefaPage.clickApagarAnotacaoButton();
        verDetalhesTarefaPage.returnMessageNaoHaAnotacao();

        Assert.assertEquals("Não há anotações anexadas a esta tarefa.", verDetalhesTarefaPage.returnMessageNaoHaAnotacao());
    }

    @Test
    public void marcarTarefaPegajosoComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickMarcarComoPegajosoButton();

        Assert.assertTrue(verDetalhesTarefaPage.returnMensagemHistoricoAtividade().contains("Tarefa \"Pegajosa\""));
    }

    @Test
    public void desmarcarTarefaPegajosoComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickMarcarComoPegajosoButton();
        verDetalhesTarefaPage.clickDesmarcarPegajosoButton();

        Assert.assertTrue(verDetalhesTarefaPage.returnMensagemHistoricoAtividade().contains("Tarefa \"Pegajosa\""));
    }

    @Test
    public void resolverTarefaComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String status = "resolvido";
        String resolucao = "corrigido";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.selecionarAlterarStatusCombobox(status);
        verDetalhesTarefaPage.clickAlterarStatusTarefaButton();
        verDetalhesTarefaPage.selecionarResolucaoTarefaComboBox(resolucao);
        verDetalhesTarefaPage.clickResolverTarefaButton();

        Assert.assertEquals(status, verDetalhesTarefaPage.returnEstadoTarefa());
    }

    @Test
    public void resolverTarefaVerificarMinhaVisao() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();
        minhaVisaoPage = new MinhaVisaoPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String status = "resolvido";
        String resolucao = "corrigido";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.selecionarAlterarStatusCombobox(status);
        verDetalhesTarefaPage.clickAlterarStatusTarefaButton();
        verDetalhesTarefaPage.selecionarResolucaoTarefaComboBox(resolucao);
        verDetalhesTarefaPage.clickResolverTarefaButton();
        String numeroTarefaResolvida = verDetalhesTarefaPage.returnNumeroTarefaModificada();
        mainPage.clickMinhaVisaoButton();

        Assert.assertEquals(numeroTarefaResolvida, minhaVisaoPage.returnTarefaResolvida(numeroTarefaResolvida));
    }

    @Test
    public void reabrirTarefaComSucesso() {
        //Preparar dados
        CriarTarefaDBSteps.criarTarefaFechadaDB();

        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String estado = "fechado";

        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickEstadoTarefaLinkText();
        verTarefasPage.selecionarEstadoComboBox(estado);
        verTarefasPage.clickAplicarFiltroButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickReabrirTarefaButton();
        verDetalhesTarefaPage.clickSolicitarRetornoButton();

        Assert.assertEquals("retorno", verDetalhesTarefaPage.returnEstadoTarefa());
    }

    @Test
    public void monitorarUmaTarefaComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickMonitorarTarefaButton();

        Assert.assertEquals(username, verDetalhesTarefaPage.returnUsuarioMonitorando());
    }

    @Test
    public void monitorarTarefaComSucessoValidarEmMinhaVisao() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();
        minhaVisaoPage = new MinhaVisaoPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickMonitorarTarefaButton();
        //Pegar numero da Tarefa monitorada
        String numeroTarefaMonitorada = verDetalhesTarefaPage.returnNumeroTarefaModificada();
        mainPage.clickMinhaVisaoButton();

        Assert.assertEquals(numeroTarefaMonitorada, minhaVisaoPage.returnTarefaMonitorada(numeroTarefaMonitorada));
    }

    @Test
    public void validarAcaoBotaoIrParaHistorico() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickIrParaHistoricoButton();

        Assert.assertTrue(verDetalhesTarefaPage.returnCampoHistoricoTarefa().contains("Histórico da Tarefa"));
    }

    @Test
    public void fecharTarefaComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickFecharButton();
        verDetalhesTarefaPage.clickFecharTarefaButton();

        Assert.assertEquals("fechado", verDetalhesTarefaPage.returnEstadoTarefa());
    }

    @Test
    public void enviarLembreteComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickEnviarLembreteButton();
        verDetalhesTarefaPage.clickUserText();
        verDetalhesTarefaPage.clickEnviarButton();

        Assert.assertEquals("Operação realizada com sucesso.", verDetalhesTarefaPage.returnMessageSucesso());
    }

    @Test
    public void validarEnvioLembrete() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickEnviarLembreteButton();
        verDetalhesTarefaPage.clickUserText();
        verDetalhesTarefaPage.clickEnviarButton();


        Assert.assertTrue(verDetalhesTarefaPage.returnAtividadesInformacao().contains("Lembrete mandado para: administrator"));
    }

    @Test
    public void enviarLembreteSemUsuarioSelecionado() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickEnviarLembreteButton();
        verDetalhesTarefaPage.clickEnviarButton();

        Assert.assertEquals("APPLICATION ERROR #200", verDetalhesTarefaPage.returnMessageError());
    }

    @Test
    public void editarTarefaComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String estado = "admitido";

        //Tests
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickAtualizarTarefaButton();
        verDetalhesTarefaPage.selecionarEstado(estado);
        verDetalhesTarefaPage.clickSalvarTarefaButton();

        Assert.assertEquals(estado, verDetalhesTarefaPage.returnEstadoTarefa(estado));
    }

    @Test
    public void editarTarefaValidarMinhaVisao() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();
        minhaVisaoPage = new MinhaVisaoPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String estado = "admitido";

        //Tests
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickAtualizarTarefaButton();
        verDetalhesTarefaPage.selecionarEstado(estado);
        verDetalhesTarefaPage.clickSalvarTarefaButton();
        //Pegar numero tarefa editada
        String numeroTarefa = verDetalhesTarefaPage.returnNumeroTarefa();
        mainPage.clickMinhaVisaoButton();

        Assert.assertEquals(numeroTarefa, minhaVisaoPage.returnNumeroTarefaModificada(numeroTarefa));
    }

    @Test
    public void alterarTarefaAtribuindoUsuario() {
        //Preparar os dados
        //CriarTarefaDBSteps.criarTarefaSemUsuarioDB();

        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();
        minhaVisaoPage = new MinhaVisaoPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String atribuidoA = "administrator";

        //Tests
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickAtualizarTarefaButton();
        verDetalhesTarefaPage.selecionarAtribuirA(atribuidoA);
        verDetalhesTarefaPage.clickSalvarTarefaButton();

        Assert.assertEquals(atribuidoA, verDetalhesTarefaPage.returnAtribuidoA());
    }

    @Test
    public void apagarTarefaComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        verTarefasPage = new VerTarefasPage();
        verDetalhesTarefaPage = new VerDetalhesTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickVerTarefasButton();
        verTarefasPage.clickRedefinirButton();
        verTarefasPage.clickTarefaLinkText();
        verDetalhesTarefaPage.clickDeletarTarefaButton();
        verDetalhesTarefaPage.clickApagarTarefaButton();

        Assert.assertEquals("", verTarefasPage.returnTarefasInformacoesTable());
    }
}
