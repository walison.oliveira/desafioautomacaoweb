package com.desafiobase2.tests;

import com.desafiobase2.bases.TestBase;
import com.desafiobase2.flows.LoginFlows;
import com.desafiobase2.pages.GerenciarMarcadoresPage;
import com.desafiobase2.pages.GerenciarPage;
import com.desafiobase2.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MarcadorTests extends TestBase {
    //Objects
    LoginFlows loginFlows;
    MainPage mainPage;
    GerenciarPage gerenciarPage;
    GerenciarMarcadoresPage gerenciarMarcadoresPage;

    @Test
    public void criarMarcadorComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarMarcadoresPage = new GerenciarMarcadoresPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeMarcador = "MarcadorTeste";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarMarcadoresButton();
        gerenciarMarcadoresPage.sendKeysNomeMarcador(nomeMarcador);
        gerenciarMarcadoresPage.clickCriarMarcador();

        Assert.assertEquals(nomeMarcador, gerenciarMarcadoresPage.returnNomeMarcador(nomeMarcador));
    }

    @Test
    public void apagarMarcadorComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarMarcadoresPage = new GerenciarMarcadoresPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarMarcadoresButton();
        gerenciarMarcadoresPage.clickNomeMarcadorLinkText();
        gerenciarMarcadoresPage.clickApagarMarcadorButton();
        gerenciarMarcadoresPage.clickApagarMarcadorButton();

        Assert.assertEquals("", gerenciarMarcadoresPage.returnInformacoesMarcadorText());
    }

    @Test
    public void alterarMarcadorComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarMarcadoresPage = new GerenciarMarcadoresPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeMarcadorAlterador = "MarcadorAlterado";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarMarcadoresButton();
        gerenciarMarcadoresPage.clickNomeMarcadorLinkText();
        gerenciarMarcadoresPage.clickAtualizarMarcadorButton();
        gerenciarMarcadoresPage.sendKeysNomeMarcadorTextField(nomeMarcadorAlterador);
        gerenciarMarcadoresPage.clickAtualizarMarcadorButton();


        Assert.assertEquals(nomeMarcadorAlterador, gerenciarMarcadoresPage.returnNomeMarcadorAlterado(nomeMarcadorAlterador));
    }
}
