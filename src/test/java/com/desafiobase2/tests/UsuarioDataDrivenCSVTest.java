package com.desafiobase2.tests;

import com.desafiobase2.bases.TestBase;
import com.desafiobase2.flows.LoginFlows;
import com.desafiobase2.pages.GerenciarPage;
import com.desafiobase2.pages.GerenciarUsuariosPage;
import com.desafiobase2.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UsuarioDataDrivenCSVTest extends TestBase {
    //Objects
    LoginFlows loginFlows;
    MainPage mainPage;
    GerenciarPage gerenciarPage;
    GerenciarUsuariosPage gerenciarUsuariosPage;

    @Test(dataProvider = "dataUsuarioCSVProvider")
    public void cadastrarUsuarioSucesso(String[] usuario) {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarUsuariosPage = new GerenciarUsuariosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeUsuario = usuario[0];
        String nomeVerdadeiro = usuario[1];
        String email = usuario[2];
        String nivelAcesso = usuario[3];

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarUsuariosButton();
        gerenciarUsuariosPage.clickCriarNovaContaButton();
        gerenciarUsuariosPage.sendKeysNomeUsuarioTextField(nomeUsuario);
        gerenciarUsuariosPage.sendKeysNomeVerdadeiro(nomeVerdadeiro);
        gerenciarUsuariosPage.sendKeysEmailTextField(email);
        gerenciarUsuariosPage.selecionarNivelAcessoCombobox(nivelAcesso);
        gerenciarUsuariosPage.clickCriarUsuarioButton();

        Assert.assertEquals("Usuário " + nomeUsuario + " criado com um nível de acesso de "+nivelAcesso, gerenciarUsuariosPage.returnMensagemSucesso());
    }

    @DataProvider(name="dataUsuarioCSVProvider")
    public Iterator<Object []> dataPetProvider2(){
        return csvProvider("src/test/java/usuarioData.csv");
    }

    //Método para leitura do arquivo .csv
    public Iterator<Object []> csvProvider(String csvNamePath) {
        String line = "";
        String cvsSplitBy = ";";
        List<Object[]> testCases = new ArrayList<>();
        String[] data = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csvNamePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                if (!((line = br.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            data = line.split(cvsSplitBy);
            testCases.add(data);
        }
        return testCases.iterator();
    }
}
