package com.desafiobase2.tests;

import com.desafiobase2.bases.TestBase;
import com.desafiobase2.flows.LoginFlows;
import com.desafiobase2.pages.GerenciarPage;
import com.desafiobase2.pages.GerenciarProjetosPage;
import com.desafiobase2.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ProjetosTests extends TestBase {
    //Objects
    LoginFlows loginFlows;
    MainPage mainPage;
    GerenciarPage gerenciarPage;
    GerenciarProjetosPage gerenciarProjetosPage;

    @Test
    public void criarProjetoComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarProjetosPage = new GerenciarProjetosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeProjeto = "ProjetoTest";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarProjetosButton();
        gerenciarProjetosPage.clickCriarNovoProjetoButton();
        gerenciarProjetosPage.sendKeysNomeProjetoTextField(nomeProjeto);
        gerenciarProjetosPage.clickAdicionarProjeto();

        Assert.assertEquals("Operação realizada com sucesso.", gerenciarProjetosPage.returnMensagemSucesso());
    }

    @Test
    public void apagarProjetoComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarProjetosPage = new GerenciarProjetosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarProjetosButton();
        gerenciarProjetosPage.clickProjetoLinkText();
        gerenciarProjetosPage.clickApagarProjetoButton();
        gerenciarProjetosPage.clickApagarProjetoButton();

        Assert.assertEquals("", gerenciarProjetosPage.returnInformacoesProjeto());
    }

    @Test
    public void alterarProjetoComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarProjetosPage = new GerenciarProjetosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeProjetoAtualizado = "Projeto Mantis Alterado";
        String estado = "release";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarProjetosButton();
        gerenciarProjetosPage.clickProjetoLinkText();
        gerenciarProjetosPage.sendKeysNomeProjetoTextField(nomeProjetoAtualizado);
        gerenciarProjetosPage.selecionarEstadoProejeto(estado);
        gerenciarProjetosPage.clickAtualizarProjetoButton();

        Assert.assertEquals(nomeProjetoAtualizado, gerenciarProjetosPage.returnNomeProjeto(nomeProjetoAtualizado));
    }

    @Test
    public void alterarCategoriaComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarProjetosPage = new GerenciarProjetosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeCategoriaAlterado = "CategoriaAlterado";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarProjetosButton();
        gerenciarProjetosPage.clickAlterarCategoriaButton();
        gerenciarProjetosPage.sendKeysCategoria(nomeCategoriaAlterado);
        gerenciarProjetosPage.clickAtualizarCategoria();

        Assert.assertEquals("Operação realizada com sucesso.", gerenciarProjetosPage.returnMensagemSucesso());
    }

    @Test
    public void validarAltecaoCategoria() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarProjetosPage = new GerenciarProjetosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeCategoriaAlteradp = "CategoriaAlterado";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarProjetosButton();
        gerenciarProjetosPage.clickAlterarCategoriaButton();
        gerenciarProjetosPage.sendKeysCategoria(nomeCategoriaAlteradp);
        gerenciarProjetosPage.clickAtualizarCategoria();

        Assert.assertEquals(nomeCategoriaAlteradp, gerenciarProjetosPage.returnNomeCategoria(nomeCategoriaAlteradp));
    }

    @Test
    public void criarCategoriaComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarProjetosPage = new GerenciarProjetosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeCategoria = "CategoriaTest";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarProjetosButton();
        gerenciarProjetosPage.sendKeysNomeCategoria(nomeCategoria);
        gerenciarProjetosPage.clickAdicionarCategoria();

        Assert.assertEquals(nomeCategoria, gerenciarProjetosPage.returnNomeCategoria(nomeCategoria));
    }

    @Test
    public void validarCampoNomeObrigatorioCriarCategoria() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarProjetosPage = new GerenciarProjetosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarProjetosButton();
        gerenciarProjetosPage.clickAdicionarCategoria();

        Assert.assertEquals("APPLICATION ERROR #11", gerenciarProjetosPage.returnMessageError());
    }

    @Test
    public void apagarCategoriaComSucesso(){
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarProjetosPage = new GerenciarProjetosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarProjetosButton();
        gerenciarProjetosPage.clickApagarCategoriaButton();
        gerenciarProjetosPage.clickConfirmarApagarCategoria();

        Assert.assertEquals("Operação realizada com sucesso.", gerenciarProjetosPage.returnMensagemSucesso());
    }
}
