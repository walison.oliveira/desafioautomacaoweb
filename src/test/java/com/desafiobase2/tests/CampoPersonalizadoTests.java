package com.desafiobase2.tests;

import com.desafiobase2.bases.TestBase;
import com.desafiobase2.flows.LoginFlows;
import com.desafiobase2.pages.GerenciarCamposPersonalizadosPage;
import com.desafiobase2.pages.GerenciarPage;
import com.desafiobase2.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CampoPersonalizadoTests extends TestBase {
    //Objects
    LoginFlows loginFlows;
    MainPage mainPage;
    GerenciarPage gerenciarPage;
    GerenciarCamposPersonalizadosPage gerenciarCamposPersonalizadosPage;

    @Test
    public void criarCampoPersolizadoComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarCamposPersonalizadosPage = new GerenciarCamposPersonalizadosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeCampoPersonalizado = "Campo_Personalizado";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarCamposPersonalizadosButton();
        gerenciarCamposPersonalizadosPage.sedKeysNomeCampoPersonalizadoTextField(nomeCampoPersonalizado);
        gerenciarCamposPersonalizadosPage.clickNovoCampoPersonalizadoButton();

        Assert.assertEquals("Operação realizada com sucesso.", gerenciarCamposPersonalizadosPage.returnMensagemSucesso());
    }

    @Test
    public void validarNomeObrigatorioCampoPersolizado() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarCamposPersonalizadosPage = new GerenciarCamposPersonalizadosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarCamposPersonalizadosButton();
        gerenciarCamposPersonalizadosPage.clickNovoCampoPersonalizadoButton();

        Assert.assertEquals("APPLICATION ERROR #11", gerenciarCamposPersonalizadosPage.returnMensagemError());
    }

    @Test
    public void alterarCampoPersolizadoComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarCamposPersonalizadosPage = new GerenciarCamposPersonalizadosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeCampoPersonalizadoAlterado = "Campo personalizado Alterado";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarCamposPersonalizadosButton();
        gerenciarCamposPersonalizadosPage.clickNomeCampoPersolizadoLinkText();
        gerenciarCamposPersonalizadosPage.sendKeysNomeCampoPersonalizadoTextField(nomeCampoPersonalizadoAlterado);
        gerenciarCamposPersonalizadosPage.clickAtualizarCampoPersonalizadoButton();

        Assert.assertEquals("Operação realizada com sucesso.", gerenciarCamposPersonalizadosPage.returnMensagemSucesso());
    }

    @Test
    public void validarAlterarCampoPersolizado() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarCamposPersonalizadosPage = new GerenciarCamposPersonalizadosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeCampoPersonalizadoAlterado = "Campo personalizado Alterado";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarCamposPersonalizadosButton();
        gerenciarCamposPersonalizadosPage.clickNomeCampoPersolizadoLinkText();
        gerenciarCamposPersonalizadosPage.sendKeysNomeCampoPersonalizadoTextField(nomeCampoPersonalizadoAlterado);
        gerenciarCamposPersonalizadosPage.clickAtualizarCampoPersonalizadoButton();

        Assert.assertEquals(nomeCampoPersonalizadoAlterado, gerenciarCamposPersonalizadosPage.returnNomeCampoPersonalizado(nomeCampoPersonalizadoAlterado));
    }

    @Test
    public void apagarCampoPersolizadoComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarCamposPersonalizadosPage = new GerenciarCamposPersonalizadosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarCamposPersonalizadosButton();
        gerenciarCamposPersonalizadosPage.clickNomeCampoPersolizadoLinkText();
        gerenciarCamposPersonalizadosPage.clickApagarCampoPersonalizadoButton();
        gerenciarCamposPersonalizadosPage.clickApagarCampoButton();

        Assert.assertEquals("Operação realizada com sucesso.", gerenciarCamposPersonalizadosPage.returnMensagemSucesso());
    }

    @Test
    public void validarApagarCampoPersolizado() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarCamposPersonalizadosPage = new GerenciarCamposPersonalizadosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarCamposPersonalizadosButton();
        gerenciarCamposPersonalizadosPage.clickNomeCampoPersolizadoLinkText();
        gerenciarCamposPersonalizadosPage.clickApagarCampoPersonalizadoButton();
        gerenciarCamposPersonalizadosPage.clickApagarCampoButton();

        Assert.assertEquals("", gerenciarCamposPersonalizadosPage.returnInformacoesCampoPersonalizadoText());
    }
}
