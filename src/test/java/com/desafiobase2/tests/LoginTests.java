package com.desafiobase2.tests;

import com.desafiobase2.bases.TestBase;
import com.desafiobase2.pages.LoginPage;
import com.desafiobase2.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends TestBase {
    //Objects
    LoginPage loginPage;
    MainPage mainPage;

    //Tests
    @Test
    public void efetuarLoginComSucesso() {
        //Objects instances
        loginPage = new LoginPage();
        mainPage = new MainPage();

        //Parameteres
        String usuario = "administrator";
        String password = "administrator";

        //Test
        loginPage.sendKeysUsernameTextField(usuario);
        loginPage.clickLoginButton();
        loginPage.senKeysPasswordTextField(password);
        loginPage.clickLoginButton();

        Assert.assertEquals(usuario, mainPage.returnUsernameInformacoesDeLogin());
    }

    @Test
    public void efetuarLoginComUsuarioInvalido() {
        //Objects instances
        loginPage = new LoginPage();

        //Parameteres
        String usuario = "usuario";
        String password = "administrator";

        //Test
        loginPage.sendKeysUsernameTextField(usuario);
        loginPage.clickLoginButton();
        loginPage.senKeysPasswordTextField(password);
        loginPage.clickLoginButton();

        Assert.assertEquals("Sua conta pode estar desativada ou bloqueada ou o " +
                "nome de usuário e a senha que você digitou não estão corretos.", loginPage.returnMessageDeErro());
    }

    @Test
    public void efetuarLoginComSenhaInvalida() {
        //Objects instances
        loginPage = new LoginPage();

        //Parameteres
        String usuario = "administrator";
        String password = "senha";

        //Test
        loginPage.sendKeysUsernameTextField(usuario);
        loginPage.clickLoginButton();
        loginPage.senKeysPasswordTextField(password);
        loginPage.clickLoginButton();

        Assert.assertEquals("Sua conta pode estar desativada ou bloqueada ou o " +
                "nome de usuário e a senha que você digitou não estão corretos.", loginPage.returnMessageDeErro());
    }

}
