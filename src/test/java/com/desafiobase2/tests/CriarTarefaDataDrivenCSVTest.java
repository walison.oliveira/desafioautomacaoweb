package com.desafiobase2.tests;

import com.desafiobase2.bases.TestBase;
import com.desafiobase2.flows.LoginFlows;
import com.desafiobase2.pages.MainPage;
import com.desafiobase2.pages.CriarTarefaPage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CriarTarefaDataDrivenCSVTest extends TestBase {
    //Objects
    LoginFlows loginFlows;
    MainPage mainPage;
    CriarTarefaPage criarTarefaPage;

    @Test(dataProvider = "dataTarefaCSVProvider")
    public void criarUmaTarefaComSucesso(String[] tarefa) {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        criarTarefaPage = new CriarTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        String categoria = tarefa[0];
        String frequencia = tarefa[1];
        String gravidade = tarefa[2];
        String prioridade = tarefa[3];
        String atribuirA = tarefa[4];
        String resumo = tarefa[5];
        String descricao = tarefa[6];
        String passosParaReproduzir = tarefa[7];
        String informacoesAdicionais = tarefa[8];

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickCriarTarefaButton();
        criarTarefaPage.clickSelecionarProjetoButton();
        criarTarefaPage.selecionarCategoria(categoria);
        criarTarefaPage.selecionarFrequencia(frequencia);
        criarTarefaPage.selecionarGravidade(gravidade);
        criarTarefaPage.selecionarPrioridade(prioridade);
        criarTarefaPage.selecionarAtribuirA(atribuirA);
        criarTarefaPage.sendKeysResumoTextField(resumo);
        criarTarefaPage.sendKeysDescricaoTextField(descricao);
        criarTarefaPage.sendKeysPassosParaReproduzirTextField(passosParaReproduzir);
        criarTarefaPage.sendKeysInformacoesAdicionaisTextField(informacoesAdicionais);
        criarTarefaPage.clickSalvarTarefaButton();

        Assert.assertEquals("Operação realizada com sucesso.", criarTarefaPage.returnMessageSucesso());
    }

    @DataProvider(name = "dataTarefaCSVProvider")
    public Iterator<Object[]> dataTarefaProvider2() {
        return csvProvider("src/test/java/tarefaData.csv");

    }

    //Método para leitura do arquivo .csv
    public Iterator<Object[]> csvProvider(String csvNamePath) {
        String line = "";
        String cvsSplitBy = ";";
        List<Object[]> testCases = new ArrayList<>();
        String[] data = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csvNamePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                if (!((line = br.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            data = line.split(cvsSplitBy);
            testCases.add(data);
        }
        return testCases.iterator();
    }

}
