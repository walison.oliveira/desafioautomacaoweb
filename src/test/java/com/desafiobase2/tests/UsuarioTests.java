package com.desafiobase2.tests;

import com.desafiobase2.bases.TestBase;
import com.desafiobase2.flows.LoginFlows;
import com.desafiobase2.pages.GerenciarPage;
import com.desafiobase2.pages.GerenciarUsuariosPage;
import com.desafiobase2.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UsuarioTests extends TestBase {
    //Objects
    LoginFlows loginFlows;
    MainPage mainPage;
    GerenciarPage gerenciarPage;
    GerenciarUsuariosPage gerenciarUsuariosPage;

    @Test
    public void cadastrarUsuarioSemInformarNomeUsuario() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarUsuariosPage = new GerenciarUsuariosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarUsuariosButton();
        gerenciarUsuariosPage.clickCriarNovaContaButton();
        gerenciarUsuariosPage.clickCriarUsuarioButton();

        Assert.assertEquals("APPLICATION ERROR #805", gerenciarUsuariosPage.returnMensagemError());
    }

    @Test
    public void cadastrarUsuarioComEmailInvalido() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarUsuariosPage = new GerenciarUsuariosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeUsuario = "UserAutomacao";
        String nomeVerdadeiro = "Teste";
        String email = "teste.com";
        String nivelAcesso = "atualizador";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarUsuariosButton();
        gerenciarUsuariosPage.clickCriarNovaContaButton();
        gerenciarUsuariosPage.sendKeysNomeUsuarioTextField(nomeUsuario);
        gerenciarUsuariosPage.sendKeysNomeVerdadeiro(nomeVerdadeiro);
        gerenciarUsuariosPage.sendKeysEmailTextField(email);
        gerenciarUsuariosPage.selecionarNivelAcessoCombobox(nivelAcesso);
        gerenciarUsuariosPage.clickCriarUsuarioButton();

        Assert.assertEquals("APPLICATION ERROR #1200", gerenciarUsuariosPage.returnMensagemError());
    }

    @Test
    public void apagarUsuarioComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarUsuariosPage = new GerenciarUsuariosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeUsuario = "automacao";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarUsuariosButton();
        gerenciarUsuariosPage.clickUsuario(nomeUsuario);
        gerenciarUsuariosPage.clickApagarUsuarioButton();
        gerenciarUsuariosPage.clickApagarContaButton();

        Assert.assertEquals("Operação realizada com sucesso.", gerenciarUsuariosPage.returnMessageSucesso());
    }

    @Test
    public void desativarUsuarioComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarUsuariosPage = new GerenciarUsuariosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeUsuario = "automacao";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarUsuariosButton();
        gerenciarUsuariosPage.clickUsuario(nomeUsuario);
        gerenciarUsuariosPage.clickHabilitadoCheckBox();
        gerenciarUsuariosPage.clickAtualizarUsarioButton();

        Assert.assertEquals("Operação realizada com sucesso.", gerenciarUsuariosPage.returnMessageSucesso());
    }

    @Test
    public void alterarUsuarioComSucesso() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarUsuariosPage = new GerenciarUsuariosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeUsuario = "automacao";
        String nomeUsuarioAlterado = "automacao2";
        String nivelAcesso = "desenvolvedor";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarUsuariosButton();
        gerenciarUsuariosPage.clickUsuario(nomeUsuario);
        gerenciarUsuariosPage.sendKeysEditNomeUsuarioTextField(nomeUsuarioAlterado);
        gerenciarUsuariosPage.selecionarNivelAcessoUsuario(nivelAcesso);
        gerenciarUsuariosPage.clickAtualizarUsarioButton();

        Assert.assertEquals("Operação realizada com sucesso.", gerenciarUsuariosPage.returnMessageSucesso());
    }

    @Test
    public void desativarUsuarioComSucessoValidarEmGerenciarContas() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        gerenciarPage = new GerenciarPage();
        gerenciarUsuariosPage = new GerenciarUsuariosPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";
        String nomeUsuario = "automacao";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickGerenciarButton();
        gerenciarPage.clickGerenciarUsuariosButton();
        gerenciarUsuariosPage.clickUsuario(nomeUsuario);
        gerenciarUsuariosPage.clickHabilitadoCheckBox();
        gerenciarUsuariosPage.clickAtualizarUsarioButton();
        gerenciarPage.clickGerenciarUsuariosButton();
        gerenciarUsuariosPage.clickMostrarDesativados();
        gerenciarUsuariosPage.clickAplicarFiltroButton();

        Assert.assertEquals(nomeUsuario, gerenciarUsuariosPage.returnUsuario(nomeUsuario));
    }
}
