package com.desafiobase2.tests;

import com.desafiobase2.bases.TestBase;
import com.desafiobase2.flows.LoginFlows;
import com.desafiobase2.pages.MainPage;
import com.desafiobase2.pages.MinhaVisaoPage;
import com.desafiobase2.pages.CriarTarefaPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CriarTarefaTests extends TestBase {
    //Objects
    LoginFlows loginFlows;
    MainPage mainPage;
    CriarTarefaPage criarTarefaPage;
    MinhaVisaoPage minhaVisaoPage;

    @Test
    public void criarUmaTarefaSemAtribuirUsuario() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        criarTarefaPage = new CriarTarefaPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        String categoria = "[Todos os Projetos] General";
        String frequencia = "aleatório";
        String gravidade = "mínimo";
        String prioridade = "normal";
        String resumo = "Tarefa Desafio";
        String descricao = "Desafio descrição";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickCriarTarefaButton();
        criarTarefaPage.clickSelecionarProjetoButton();
        criarTarefaPage.selecionarCategoria(categoria);
        criarTarefaPage.selecionarFrequencia(frequencia);
        criarTarefaPage.selecionarGravidade(gravidade);
        criarTarefaPage.selecionarPrioridade(prioridade);
        criarTarefaPage.sendKeysResumoTextField(resumo);
        criarTarefaPage.sendKeysDescricaoTextField(descricao);
        criarTarefaPage.clickSalvarTarefaButton();

        Assert.assertEquals("", criarTarefaPage.returnAtribuidoAText());
    }

    @Test
    public void criarTarefaSemAtribuirValidarEmMinhaVisao() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        criarTarefaPage = new CriarTarefaPage();
        minhaVisaoPage = new MinhaVisaoPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        String categoria = "[Todos os Projetos] General";
        String frequencia = "aleatório";
        String gravidade = "mínimo";
        String prioridade = "normal";
        String resumo = "Tarefa Desafio";
        String descricao = "Desafio descrição";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickCriarTarefaButton();
        criarTarefaPage.selecionarCategoria(categoria);
        criarTarefaPage.selecionarFrequencia(frequencia);
        criarTarefaPage.selecionarGravidade(gravidade);
        criarTarefaPage.selecionarPrioridade(prioridade);
        criarTarefaPage.sendKeysResumoTextField(resumo);
        criarTarefaPage.sendKeysDescricaoTextField(descricao);
        criarTarefaPage.clickSalvarTarefaButton();
        mainPage.clickMinhaVisaoButton();

        Assert.assertEquals(resumo, minhaVisaoPage.returnTarefaNaoAtribuida(resumo));
    }

    @Test
    public void criarTarefaAtribuirAMimValidarEmMinhaVisao() {
        //Objects instances
        loginFlows = new LoginFlows();
        mainPage = new MainPage();
        criarTarefaPage = new CriarTarefaPage();
        minhaVisaoPage = new MinhaVisaoPage();

        //Parameters
        String username = "administrator";
        String password = "administrator";

        String categoria = "[Todos os Projetos] General";
        String frequencia = "aleatório";
        String gravidade = "mínimo";
        String prioridade = "normal";
        String atribuirA = "administrator";
        String resumo = "Tarefa Desafio";
        String descricao = "Desafio descrição";

        //Test
        loginFlows.efetuarLogin(username, password);
        mainPage.clickCriarTarefaButton();
        criarTarefaPage.selecionarCategoria(categoria);
        criarTarefaPage.selecionarFrequencia(frequencia);
        criarTarefaPage.selecionarGravidade(gravidade);
        criarTarefaPage.selecionarPrioridade(prioridade);
        criarTarefaPage.selecionarAtribuirA(atribuirA);
        criarTarefaPage.sendKeysResumoTextField(resumo);
        criarTarefaPage.sendKeysDescricaoTextField(descricao);
        criarTarefaPage.clickSalvarTarefaButton();
        //Pegar numero tarefa criada
        String numeroTarefa = criarTarefaPage.returnNumeroTarefa();
        mainPage.clickMinhaVisaoButton();

        Assert.assertEquals(numeroTarefa, minhaVisaoPage.returnTarefasAtribuidosAMim(numeroTarefa));
    }
}
