package com.desafiobase2.bases;

import com.desafiobase2.GlobalParameters;
import com.desafiobase2.dbsteps.*;
import com.desafiobase2.utils.DriverFactory;
import com.desafiobase2.utils.ExtentReportUtils;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;

public class TestBase {
    @BeforeSuite
    public void beforeSuite(){
        new GlobalParameters();
        ExtentReportUtils.createReport();
    }

    @BeforeMethod
    public void beforeTest(Method method){
        ExtentReportUtils.addTest(method.getName(), method.getDeclaringClass().getSimpleName());
        DriverFactory.createInstance();
        DriverFactory.INSTANCE.manage().window().maximize();
        DriverFactory.INSTANCE.navigate().to(GlobalParameters.URL_DEFAULT);
        LimparDadosBancoDBSteps.limparDadosBD();

        //=====================Preparando a massa de dados=====================
        //Criar projeto para teste
        CriarProjectDBSteps.criarProjectDB();

        //Criar campo personalizado para teste
        CriarCampoPersonalizadoDBSteps.criarCampoPersonalizadoDB();

        //Criar marcador para teste
        CriarMarcadorDBSteps.criarMarcadorDB();

        //Criar categoria para teste
        CriarCategoryDBSteps.criarCategoryDB();

        //Criar Tarefas  e anotações para Teste
        CriarTarefaDBSteps.criarTarefaDB();
        CriarAnotacaoDBSteps.criarAnotacaoDB();

        //Criar usuário para teste
        CriarUsuarioDBSteps.criarUsuarioDB();
    }

    @AfterMethod
    public void afterTest(ITestResult result){
        ExtentReportUtils.addTestResult(result);
        DriverFactory.quitInstace();
    }

    @AfterSuite
    public void afterSuite(){
        ExtentReportUtils.generateReport();
    }
}