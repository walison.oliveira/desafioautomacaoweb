## Projeto criado tendo como base o template de Automação da Base2

## Automação

- Arquitetura Projeto
	- Linguagem	- [Java](https://www.java.com/pt-BR// "Java")
	- [Java Kit Development versão 8](https://www.oracle.com/br/java/technologies/javase/javase-jdk8-downloads.html)
	- Gestão de dependências - [Maven](https://maven.apache.org)
	- Framework de Testes automatizass web - [Selenium.WebDriver 3.141](https://www.seleniumhq.org/download/ "Selenium.WebDriver")
	- Orquestrador de testes - [TestNG](https://testng.org/doc/ "TestNG")
	- Relatório de testes automatizados - [ExtentReports 4.0.9](http://www.extentreports.com/docs/versions/4/java/index.html "ExtentReports 4.0.9")

**Relatório de testes**

Após toda execução: sucesso ou falha, é gerado um relatório com cada passo realizado do teste. Está disponível na pasta do projeto "target/reports". Existe a possibilidade de tirar screenshots em cada passo ou somente em caso de falha. Verifique o arquivo globalParameteres.properties e coloque true ou false no parâmetro: get.screenshot.for.each.step.

Obs: a imagens possuem path dinâmico, basta zipar a pasta do relatório e enviar para o destinatário ou subir em algum bucket AWS para acessar que todos os dados estarão consistentes.


**Padrões por tipo de componente**

* Pastas: começam sempre com letra minúscula. Ex: `pages, dbsteps, basess`
* Classes: começam sempre com letra maiúscula. Ex: `LoginPage, LoginTests`
* Arquivos: começam sempre com letra minúscula. Ex: `report.png`
* Métodos: começam sempre com letra minúscula. Ex: `efetuarLoginComSucesso()`
* Variáveis: começam sempre com letra minúscula. Ex: `botaoXPTO`
* Objetos: começam sempre com letra minúscula. Ex: `loginPage`


**Padrões de escrita: Classes e Arquivos**

Nomes de classes e arquivos devem terminar com o tipo de conteúdo que representam, em inglês, ex:

```
LoginPage (classe de PageObjects)
LoginTests (classe de testes)
LoginTestData.csv (planilha de dados)
```

OBS: Atenção ao plural e singular! Se a classe contém um conjunto do tipo que representa, esta deve ser escrita no plural, caso seja uma entidade, deve ser escrita no singular.


**Padrões de escrita: Geral**

Nunca utilizar acentos, espaços, caracteres especiais e “ç” para denominar pastas, classes, métodos, variáveis, objetos e arquivos.

**Padrões de escrita: Objetos**

Nomes dos objetos devem ser exatamente os mesmos nomes de suas classes, iniciando com letra minúscula, ex:

```
LoginPage (classe) loginPage (objeto)
LoginFlows (classe) loginFlows (objeto)
```
================================================================================

## O projeto ficou estruturado da seguinte forma

**Ambiente Mantis criado em docker**

```
version: "3"
services:
  db: 
    environment: 
      - MYSQL_ROOT_PASSWORD=root
      - MYSQL_DATABASE=bugtracker
      - MYSQL_USER=mantisbt
      - MYSQL_PASSWORD=mantisbt
    image: 
      mariadb
    ports: 
      - "3306:3306"
    restart:
      always
  mantisbt:
    image: "vimagick/mantisbt:latest"
    links: 
      - db
    ports: 
      - "8989:80"
    restart:
      always
     
  chrome:
    image: selenium/node-chrome:4.1.1-20211217
    shm_size: 2gb
    depends_on:
      - selenium-default-hub
    environment:
      - SE_EVENT_BUS_HOST=selenium-default-hub
      - SE_EVENT_BUS_PUBLISH_PORT=4442
      - SE_EVENT_BUS_SUBSCRIBE_PORT=4443
      - SE_NODE_GRID_URL=http://localhost:4444  
      - SE_NODE_SESSION_TIMEOUT=30
      - SE_SESSION_REQUEST_TIMEOUT=50
      - SCREEN_WIDTH=1920
      - SCREEN_HEIGHT=1080

  edge:
    image: selenium/node-edge:4.1.1-20211217
    shm_size: 2gb
    depends_on:
      - selenium-default-hub
    environment:
      - SE_EVENT_BUS_HOST=selenium-default-hub
      - SE_EVENT_BUS_PUBLISH_PORT=4442
      - SE_EVENT_BUS_SUBSCRIBE_PORT=4443
      - SE_NODE_GRID_URL=http://localhost:4444
      - SE_NODE_SESSION_TIMEOUT=30 
      - SE_SESSION_REQUEST_TIMEOUT=50
      - SCREEN_WIDTH=1920
      - SCREEN_HEIGHT=1080

  firefox:
    image: selenium/node-firefox:4.1.1-20211217
    shm_size: 2gb
    depends_on:
      - selenium-default-hub
    environment:
      - SE_EVENT_BUS_HOST=selenium-default-hub
      - SE_EVENT_BUS_PUBLISH_PORT=4442
      - SE_EVENT_BUS_SUBSCRIBE_PORT=4443
      - SE_NODE_GRID_URL=http://localhost:4444
      - SE_NODE_SESSION_TIMEOUT=30 
      - SE_SESSION_REQUEST_TIMEOUT=50
      - SCREEN_WIDTH=1920
      - SCREEN_HEIGHT=1080

  selenium-default-hub:
    image: selenium/hub:4.1.1-20211217
    container_name: selenium-default-hub
    ports:
      - "4442:4442"
      - "4443:4443"
      - "4444:4444"
```

com.desafiobase2.bases
```
PageBase (Contém as ações das pages: como click, sendkeys e gettext)
TestBase (Contém as ações para executar os testes) 
	- O @BeforeMethod, o qual executará ações antes de executar o método de teste, como por 
	exemplo preparar a massa de dados no Banco de Dados e iniciar a instância do driver.
	- O @AfterMethod, o qual executará ações após a execução do método de teste,
	como por exemplo finalizar a instância do driver
	
```

com.desafiobase2.dbsteps
```
Contém as que serão executadas no banco de dados, para preparar a massa de dados
```

com.desafiobase2.flows
```
Contém uma ação que se repete várias vezes
```

com.desafiobase2.pages.criartarefapage
```
Contém o mapeamento e as ações que serão executadas na página de Criar tarefas
```

com.desafiobase2.pages.gerenciarcampopersonalizadopage
```
Contém o mapeamento e as ações que serão executadas na página de Gerenciar Campos personalizados
```

com.desafiobase2.pages.gerenciarmarcadorespage
```
Contém o mapeamento e as ações que serão executadas na página de Gerenciar Marcadores
```

com.desafiobase2.pages.gerenciarpage
```
Contém o mapeamento e as ações que serão executadas na página de Gerenciar
```

com.desafiobase2.pages.gerenciarprojetospage
```
Contém o mapeamento e as ações que serão executadas na página de Gerenciar Projetos
```

com.desafiobase2.pages.gerenciarusuariospage
```
Contém o mapeamento e as ações que serão executadas na página de Gerenciar Usuários
```

com.desafiobase2.pages.loginpage
```
Contém o mapeamento e as ações que serão executadas na página de Login
```

com.desafiobase2.pages.mainpage
```
Contém o mapeamento e as ações que serão executadas na página principal
```

com.desafiobase2.pages.minhavisaopage
```
Contém o mapeamento e as ações que serão executadas na página Minha Visão
```

com.desafiobase2.pages.verdetalhestarefaspage
```
Contém o mapeamento e as ações que serão executadas na página de Ver Detalhes Tarefas
```

com.desafiobase2.pages.vertarefaspage
```
Contém o mapeamento e as ações que serão executadas na página de Ver Tarefas
```


com.desafiobase2.queries.gerenciar
```
Responsável pelas queries de SQL executadas nos testes de gerenciar
```

com.desafiobase2.queries.limpardadosbanco
```
Responsável pelas queries de SQL executadas antes dos métodos, para limpar o lixo do banco
```

com.desafiobase2.queries.tarefa
```
Responsável pelas queries de SQL executadas nos testes de tarefa
```

com.desafiobase2.tests.campopersonalizadotests
```
Responsável pela execução dos testes da rotina de Campos personalizados
```

com.desafiobase2.tests.criartarefatests
```
Responsável pela execução dos testes da rotina de Criar Tarefas e Ver Tarefas
```

com.desafiobase2.tests.logintests
```
Responsável pela execução dos testes da rotina de Login (Testes realizados com JavaScript) 
```

com.desafiobase2.tests.marcadorestests
```
Responsável pela execução dos testes da rotina de Marcadores
```

com.desafiobase2.tests.projetostests
```
Responsável pela execução dos testes da rotina de Projeto
```

com.desafiobase2.tests.usuariostests
```
Responsável pela execução dos testes da rotina de Usuários
```

com.desafiobase2.tests.vertarefatests
```
Responsável pela execução dos testes da rotina de Ver Tarefas
```


globalParameters.properties
```
Responsável por gerenciar os parametros de execução. 

browser.default=chrome (browser a ser executado)
execution=local (execução remota ou localmente
timeout.default=30  
selenium.hub=http://localhost:4444/wd/hub (caminho pa)
url.default=http://192.168.2.121:8989 (url da aplicação a ser testada)
report.name=desafiobase2 (nome relatorio)
get.screenshot.for.each.step=true (valor = true ou false. True, retira 
screenshots durante a execução de cada método de teste. False, retira 
screenshots em cada método de teste que falhar)
download.defaul.path=C:/
report.path=target/reports/ (local de armazenamento dos relatorios e screenshot)
db.url=jdbc:mysql://localhost:3306/bugtracker (caminho banco)
db.sid=null
db.user=root (usuário banco)
db.password=root (senha banco)
```

**1- Foram executados 50 casos de teste**

Responsável por executar a suite de testes

```
testngsuite.xml
```

**2- Realizado o Data-Driven nas Classes 'criarUsuarioDataDrivenCSVTest' e 'CriarTarefaDataDrivenCSVTest'**

As classes utilizaram os seguintes arquivos

```
tarefaData.csv
usuarioData.csv
```

**3- Os casos de testes podem ser executados em 3 navegadores (Firefox, Google Chrome, Edge)**

Ajuste realizado em globalParameters.properties, caso a execução for remota ou local

```
browser.default=chrome (firefox, chrome e edge)
execution=remota (local ou remota)
```

Utilizando o Selenium GRID com container docker possível executar nos três navegadores
![alt text](https://i.imgur.com/oLV0VPK.jpg)

Exemplo de execução
![alt text](https://imgur.com/0cXQLsZ.jpg)

**4- Utilizado o ExtentReport para gerar o relatório de teste com screenshots**

Relatórios e screnshots são armazenados em:
```
target/reports/
```
**5- O Projeto prepara a massa de dados com scripts SQL**

Scripts armazenados em:
```
com.desafiobase2.queries
```

**6- Os testes da classe 'loginTests' são executados utilizando de JavaScript**

**7- Projeto agendado no Azure DevOps**

Execução:
![alt text](https://i.imgur.com/CTJxOTi.jpg)

![alt text](https://i.imgur.com/PrsOfki.jpg)

Relatório e Screenshot:
![alt text](https://i.imgur.com/17YB6i5.jpg)

![alt text](https://i.imgur.com/Lutgrck.jpg)

![alt text](https://i.imgur.com/l5bs0Pw.jpg)

![alt text](https://imgur.com/akexshN.jpg)